package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio7EstructurasdeControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 7 de Operadores
		
		String lenguaje;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Pon un lenguaje <C++, Java, JavaScript>: ");
		lenguaje = readFromConsole.next();
		
		if (lenguaje.equals("C++")) {
			System.out.println("El mejor lenguaje");
		}else if(lenguaje.equals("Java")) {
			System.out.println("El segundo mejor lenguaje");
		}
		else if (lenguaje.equals("JavaScript")) {
			System.out.println("El lenguaje actual");
		} else {
			System.out.println("Nada que decir");
		}
	}
}

