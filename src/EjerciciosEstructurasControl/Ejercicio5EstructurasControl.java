package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio5EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 5 de Estructuras de Control
		
		int n1;
		int n2;
		
	    Scanner readFromConsole = new Scanner(System.in);
	    System.out.print("Introduce un n�mero: ");
	    n1 = readFromConsole.nextInt();
	    System.out.print("Introduce otro n�mero: ");
	    n2 = readFromConsole.nextInt();
	    if(n1%n2==0)
	    System.out.println(n1+" es m�ltiplo de "+n2);
	    else
	    System.out.println(n1+" no es m�ltiplo de "+n2);

	}

}
