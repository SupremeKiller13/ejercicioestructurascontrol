package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio3EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 3 de Estructuras de Control
		
		int numero = 0;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		numero = readFromConsole.nextInt();
		if (numero%2==0)
		    System.out.println("El n�mero "+numero+" es par");
		else
		    System.out.println("El n�mero "+numero+" es impar");
		if (numero > 0) {
			System.out.println("El n�mero "+numero+" es positivo");
		}else {
			System.out.println("El n�mero "+numero+" es negativo");
		}

	}

}
