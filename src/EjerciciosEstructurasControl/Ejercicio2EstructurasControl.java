package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio2EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 2 de Estructuras de Control
		
		int numero = 0;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		numero = readFromConsole.nextInt();
		if (numero == 0) {
			System.out.println("El n�mero es 0");
		}else if(numero > 0) {
			System.out.println("El n�mero es positivo");
		}
		else {
			System.out.println("El n�mero es negativo");
		}
	}

}
