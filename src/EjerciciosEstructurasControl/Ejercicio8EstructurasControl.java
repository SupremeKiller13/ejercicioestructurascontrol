package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio8EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 8 de Estructuras de Control
		
		int numero;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero entre 0 y 99: ");
		numero = readFromConsole.nextInt();
		
		switch (numero) {
		case 1:
		{
			System.out.println("Eres Keeper");
		}
			break;
		case 2: case 3: case 4:
			System.out.println("Eres Defender");
			break;
		case 6: case 8: case 11:
			System.out.println("Eres Midfield");
			break;
		case 9:
			System.out.println("Eres Striker");
			break;

		default:
			System.out.println("Any");
			break;
		}

	}
		
}