package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio1EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 1 de Estructuras de Control
		
		int numero = 0;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		numero = readFromConsole.nextInt();
		if (numero%2==0)
		    System.out.println("El n�mero es par");
		else
		    System.out.println("El n�mero es impar");

	}

}
