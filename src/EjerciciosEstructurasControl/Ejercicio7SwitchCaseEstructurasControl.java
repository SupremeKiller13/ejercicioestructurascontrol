package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio7SwitchCaseEstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 7 de Estructuras de Control pero con SwitchCase
		
		String lenguaje;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Pon un lenguaje <C++, Java, JavaScript>: ");
		lenguaje = readFromConsole.next();
		
		switch (lenguaje) {
		case "C++":
		{
			System.out.println("El mejor lenguaje");
		}
			break;
		case "Java":
		{
			System.out.println("El segundo mejor lenguaje");
		}
			break;
		case "JavaScript":
		{
			System.out.println("El lenguaje actual");
		}
			break;
		default:
			System.out.println("Nada que decir");
			break;
		}

	}
		
}