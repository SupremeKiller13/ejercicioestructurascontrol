package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio6EstructurasdeControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 6 de Operadores
		// Masa Corporal = peso dividido por la altura al cuadrado
		
		double peso;
		double altura;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Pon tu peso en Kilogramos: ");
		peso = readFromConsole.nextDouble();
		
		System.out.println("Pon tu altura en Cent�metros: ");
		altura = readFromConsole.nextDouble();
		
		System.out.println("Tu peso es: "+peso+" Kg");
		System.out.println("Tu altura es: "+altura+" Cm");
		
		double masa = peso / (altura*altura)*10000;
		System.out.println("Tu Indice de Masa Corporal es: "+masa);
		
		if (masa < 16) {
			System.out.println("Necesitas comer m�s");
		} else if (masa >= 16 && masa < 25) {
			System.out.println("Estas bien");
		} else if (masa > 25 && masa < 30) {
		System.out.println("Comes mucho");
		}else if (masa > 30) {
		System.out.println("Ve al hospital");
			}
		}
		

		}
