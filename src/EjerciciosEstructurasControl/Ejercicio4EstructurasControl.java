package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio4EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 4 de Estructuras de Control
		
		int n1;
		int n2;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		n1 = readFromConsole.nextInt();
		System.out.println("Introduce otro n�mero: ");
		n2 = readFromConsole.nextInt();
		if (n1 > n2) {
			System.out.println(n1+" es mayor a "+n2);
		}else if(n1 == n2) {
			System.out.println(n1+" es igual que "+n2);
		}
		else {
			System.out.println(n1+" es menor que "+n2);
		}
	}

}
