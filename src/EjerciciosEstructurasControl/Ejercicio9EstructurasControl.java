package EjerciciosEstructurasControl;

import java.util.Scanner;

public class Ejercicio9EstructurasControl {

	public static void main(String[] args) {
		// Este es el Ejercicio 9 de Estructuras de Control
		
		Scanner readFromConsole = new Scanner(System.in);
		 double cantidad;
		 String moneda;
		 double conversion = 0;
		 
		System.out.println("Escribe una cantidad en euros: ");
		cantidad = readFromConsole.nextDouble();
		
        System.out.println("Escribe la moneda a la que quieres convertir<dolares/libras/yenes>: ");
        moneda = readFromConsole.next();
		 
        switch (moneda){
	        case "dolares":
	        	conversion=cantidad*1.18;
	            break;
	        case "libras":
	        	conversion=cantidad*1.12551467;
	            break;
	        case "yenes":
	        	conversion=cantidad*132.3585;
	            break;
	        default:
	            System.out.println("No has introducido una moneda correcta");
	            break;
	        }
        System.out.println(cantidad+ " euros en " +moneda+ " son " +conversion);

	}

}
